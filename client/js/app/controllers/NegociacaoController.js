class NegociacaoController {

  constructor() {
    let $ = document.querySelector.bind(document);
  
    this._inputData = $('#data');
    this._inputQuantidade = $('#quantidade');
    this._inputValor = $('#valor');

    this._listaNegociacoes = new Bind(
      new ListaNegociacoes(),
      new NegociacoesView($('#negociacoesView')),
      'adiciona','esvazia'
    );

    this._mensagem = new Bind(
      new Mensagem(),
      new MensagemView($('#mensagemView')),
      'texto',
    );
  }

  adiciona(event) {
    event.preventDefault();

    this._listaNegociacoes.adiciona(this._criaNegociacao());
    this._mensagem.texto = 'Negociação adicionada com sucesso!';
    
    this._limpaMensagem();  
    this._limpaFormulario();
  }

  importaNegociacoes() {
    let negociacaoService = new NegociacaoService();
    Promise.all([
      negociacaoService.obtemNegociacoes('semana'),
      negociacaoService.obtemNegociacoes('anterior'),
      negociacaoService.obtemNegociacoes('retrasada')
    ])
      .then(response => {
        let negociacoes = response.flat();
        negociacoes.forEach(negociacao => this._listaNegociacoes.adiciona(negociacao));
        this._mensagem.texto = 'Negociações importadas com sucesso.';
      })
      .catch(error => this._mensagem.texto = error);



  };

  
  apaga() {
    this._listaNegociacoes.esvazia();
    this._mensagem.texto = 'Negociações apagadas com sucesso!';
    this._limpaMensagem();
  }

  _limpaMensagem() {
    setTimeout(() => {
      this._mensagem.texto = '';
    },2000);
  }

  _criaNegociacao() {
    return new Negociacao(
      DateHelper.textoParaData(this._inputData.value),
      this._inputQuantidade.value,
      this._inputValor.value
    );
  }

  _limpaFormulario() {

    this._inputData.value = '';
    this._inputQuantidade.value = 1;
    this._inputValor.value = 0.0;
    this._inputData.focus();
  }
}