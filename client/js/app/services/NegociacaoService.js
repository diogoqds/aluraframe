class NegociacaoService {

  obtemNegociacoes(route = 'semana') {

    return new Promise((resolve, reject) => {
      let xhr = new XMLHttpRequest();

      xhr.open('GET', `/negociacoes/${route}`);

      xhr.onreadystatechange = () => {
        if (xhr.readyState == 4) {
          
          if(xhr.status == 200) {
            let response = JSON.parse(xhr.responseText);
            let negociacoes = response.map(negociacaoObject => 
              new Negociacao(new Date(negociacaoObject.data),negociacaoObject.quantidade,negociacaoObject.valor)
            )
            resolve(negociacoes);
          } else {
            let response = JSON.parse(xhr.responseText);
            console.log(response)
            reject(`Não foi possível importar as negociações da semana ${route}.`);
          }
        }
      } 

      xhr.send();
    });
  }
}